# Jinja Info

## A list of sites to get us started
* Template Designer Documentation
  * https://jinja.palletsprojects.com/en/2.10.x/templates/
* Main Jinja site
  * https://jinja.palletsprojects.com/en/2.10.x/
* Flask and Jinja2 SSTI - Cheatsheet *Aimed at Security Testing Jinja and Sandboxes*
  * https://www.hacktoday.io/t/flask-jinja2-ssti-cheatsheet/2259

## Tutorials
* A Primer on Jinja Templating
  * https://realpython.com/primer-on-jinja-templating/
* Jinja Templating Tutorial
  * https://pythonprogramming.net/jinja-template-flask-tutorial/
